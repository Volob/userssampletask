import { createRouter, createWebHashHistory } from 'vue-router'
import store from '../store';
import Users from '../pages/Users.vue';

const routes = [
  {
    path: '/',
    name: 'Index',
    redirect: '/users'
  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../pages/Login.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to) => {
  if(to.meta.requiresAuth && !store.getters['user/isAuthenticated']){
    return { name: 'Login' }
  }
})

export default router
