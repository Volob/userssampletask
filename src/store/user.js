export default {
  namespaced: true,
  state: () => ({
    user: JSON.parse(sessionStorage.getItem('user')) || null
  }),

  getters: {
    isAuthenticated: state => !!state.user
  },

  mutations: {
    setUser(state, payload) {
      sessionStorage.setItem('user', JSON.stringify(payload));
      state.user = payload;
    }
  }
}
