const path = require('path')

module.exports = {
  configureWebpack: {
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '@store': path.resolve(__dirname, 'src/store'),
            '@components': path.resolve(__dirname, 'src/components')
        },
    }
  }
}